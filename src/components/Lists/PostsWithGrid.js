import { useEffect, useState, useContext, useMemo } from "react"
import { Table, Button } from 'reactstrap'
import { AgGridReact } from 'ag-grid-react'
import { CellClickedEvent } from "ag-grid-community";

import { consts, QUERY_KEYS } from "../../config/consts"
import { useModal, usePosts } from '../../RQ/Queries'
import EditButtons from '../utils/EditButtons'



function Posts() {

    const { status, data, error, isFetching, isLoading } = usePosts()

    const [columnDefs] = useState([
        { field: consts.COLUMN_ID },
        { field: consts.COLUMN_USER_ID },
        { field: consts.COLUMN_TITLE },
        { field: consts.COLUMN_BODY },
        {
            field: consts.COLUMN_ACTIONS,
            cellRenderer: EditButtons,
        },
    ])


    if (isFetching)
        return <div>FETCHING ... </div>

    const rowData = data.map((post, index) => {
        return {
            [consts.COLUMN_ID]: post.id,
            [consts.COLUMN_USER_ID]: post.userId,
            [consts.COLUMN_TITLE]: post.title,
            [consts.COLUMN_BODY]: post.body,
            [consts.COLUMN_ACTIONS]: {listType: consts.TYPE_POST, repo: post},
        }
    })

    const onCellClicked = (event: CellClickedEvent) => {
        console.log('Clicked on Grid Item')
    }

    return (
        <>

            <div className="ag-theme-alpine" style={{ height: 400, width: 1000 }}>
                <AgGridReact
                    rowData={rowData}
                    columnDefs={columnDefs}
                    enableRtl={true}
                    onCellClicked={onCellClicked}
                // components={components}
                >
                </AgGridReact>
            </div>

        </>
    )
}

export default Posts
