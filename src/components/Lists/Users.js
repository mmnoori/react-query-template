// import { useEffect } from 'react';
// import { Table } from 'reactstrap';
//
//
// import { QUERY_KEYS } from '../../config/consts'
// import { useGetFetchQuery, useUsers } from '../../RQ/Queries'
//
//
// function Users() {
//
//     // Check is users are already fetched
//     const fetchedUsers = useGetFetchQuery(QUERY_KEYS.Users)
//
//     let usersQuery
//
//     // Can't use if confition
//     // React Hooks must be called in the exact same order in every component render
//     usersQuery = useUsers(fetchedUsers);
//
//
//
//
//     console.log(usersQuery)
//
//
//     if (usersQuery.isFetching)
//         return <div>FETCHING ... </div>
//
//     const users = usersQuery.data.map((user, index) =>
//         <tr className="table-primary" key={index}>
//             <th scope="row">{user.id}</th>
//             <td>{user.name}</td>
//             <td>{user.username}</td>
//             <td>{user.email}</td>
//             <td>{user.phone}</td>
//             <td>{user.website}</td>
//         </tr>
//     )
//
//     // console.log('Log From Users Component : ')
//     // console.log(temp)
//     // console.log('\n\n')
//
//
//     // useEffect(() => { }, []);
//
//
//     return (
//         <div className="table-responsive table-con">
//
//             <Table className="table-main">
//                 <thead>
//                     <tr>
//                         <th>
//                             آیدی
//                         </th>
//                         <th>
//                             نام
//                         </th>
//                         <th>
//                             یوزرنیم
//                         </th>
//                         <th>
//                             ایمیل
//                         </th>
//                         <th>
//                             شماره تماس
//                         </th>
//                         <th>
//                             وبسایت
//                         </th>
//                     </tr>
//                 </thead>
//
//                 <tbody>
//
//                     {users}
//
//                 </tbody>
//
//             </Table>
//         </div>
//     );
// }
//
// export default Users;
