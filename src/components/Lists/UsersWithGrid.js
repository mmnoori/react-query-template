import { useEffect, useState, useContext, useMemo } from "react"
import axios from 'axios'
import { Table, Button } from 'reactstrap'
import { AgGridReact } from 'ag-grid-react'
import { CellClickedEvent } from "ag-grid-community"

import { consts, QUERY_KEYS } from "../../config/consts"
import {  useUsers } from '../../RQ/Queries'
import EditButtons from '../utils/EditButtons'



function Users() {

    const { status, data, error, isFetching, isLoading } = useUsers()

    const [columnDefs] = useState([
        { field: consts.COLUMN_ID },
        { field: consts.COLUMN_NAME },
        { field: consts.COLUMN_USERNAME },
        { field: consts.COLUMN_EMAIL },
        { field: consts.COLUMN_PHONE },
        { field: consts.COLUMN_WEBSITE },
        {
            field: consts.COLUMN_ACTIONS,
            cellRenderer: EditButtons,
        },
    ])

    if (isFetching)
        return <div>FETCHING ... </div>

    const rowData = data.map((user, index) => {
        return {
            [consts.COLUMN_ID]: user.id,
            [consts.COLUMN_NAME]: user.name,
            [consts.COLUMN_USERNAME]: user.username,
            [consts.COLUMN_EMAIL]: user.email,
            [consts.COLUMN_PHONE]: user.phone,
            [consts.COLUMN_WEBSITE]: user.website,
            [consts.COLUMN_ACTIONS]: {listType: consts.TYPE_USER, repo: user},
        }
    })

    const onCellClicked = (event: CellClickedEvent) => {
        console.log('Clicked on Grid Item')
    }

    return (
        <>

            <div className="ag-theme-alpine" style={{ height: 400, width: 1000 }}>
                <AgGridReact
                    rowData={rowData}
                    columnDefs={columnDefs}
                    enableRtl={true}
                    onCellClicked={onCellClicked}
                    // components={components}
                >
                </AgGridReact>
            </div>

        </>
    )
}

export default Users
