// import {useEffect, useState, useContext} from "react";
// import axios from 'axios'
// import {Table, Button} from 'reactstrap';
//
// import {consts, QUERY_KEYS} from "../../config/consts";
// import {useModal, usePosts} from '../../RQ/Queries'
// import {ModalContext, FormContext} from '../../config/contexts'
//
// import {
//     useQuery,
//     useMutation,
//     useQueryClient,
//     QueryClient,
//     QueryClientProvider,
// } from 'react-query'
//
//
// function Posts() {
//
//     const queryClient = useQueryClient();
//     const {status, data, error, isFetching, isLoading} = usePosts()
//
//
//     const {modalData, setModalData} = useContext(ModalContext)
//
//     function openEditPostModal(post) {
//         setModalData({state: consts.MODAL_OPENED, modalType: consts.MODAL_EDIT_POST, repo: {...post}})
//     }
//
//     function openDeletePostModal(post) {
//         setModalData({state: consts.MODAL_OPENED, modalType: consts.MODAL_DELETE, repo: {...post}})
//     }
//
//
//     // React Hooks must be called in a React function component or a custom React Hook function
//
//
//     if (isLoading)
//         return <div>LOADING ... </div>
//     else if (isFetching)
//         return <div>FETCHING ... </div>
//
//
//     const posts = data.map((post, index) =>
//         <tr className="table-primary" key={index}>
//             <th scope="row">{post.userId}</th>
//             <td>{post.id}</td>
//             <td>{post.title}</td>
//             <td>{post.body}</td>
//             <td className="table-buttons-con">
//
//                 <Button onClick={() => openEditPostModal(post)}>ادیت</Button>
//                 <Button onClick={() => openDeletePostModal(post)}>حذف</Button>
//
//             </td>
//         </tr>
//     )
//
//     return (
//         <>
//
//             <div className="table-responsive table-con">
//
//                 <Table className="table-main">
//
//                     <thead>
//                     <tr>
//                         <th>
//                             آیدی
//                         </th>
//                         <th>
//                             یوزر آیدی
//                         </th>
//                         <th>
//                             تایتل
//                         </th>
//                         <th>
//                             متن
//                         </th>
//                         <th>
//                             اکشن
//                         </th>
//                     </tr>
//                     </thead>
//
//                     <tbody>
//
//                     {posts}
//
//                     </tbody>
//
//                 </Table>
//
//             </div>
//
//         </>
//     );
// }
//
//
// export default Posts