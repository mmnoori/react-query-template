import React, { Component, useState } from 'react';
import { Routes, Route, Outlet, Link, useLocation } from "react-router-dom";

import { ListGroup, ListGroupItem, Row, Col, Nav, NavItem, NavLink, Button } from 'reactstrap';
import { ROUTES } from '../config/consts';


function Navabar() {

    const { pathname } = useLocation();

    return (
        <ListGroup className='sidebar'>

            <ListGroupItem active={pathname === ROUTES.HOME}>
                <Link to={ROUTES.HOME}>
                    منو
                </Link>
            </ListGroupItem>

            <ListGroupItem active={pathname === ROUTES.USERS}>
                <Link to={ROUTES.USERS}>
                    کاربران
                </Link>
            </ListGroupItem>

            <ListGroupItem active={pathname === ROUTES.POSTS}>
                <Link to={ROUTES.POSTS}>
                    پست ها
                </Link>
            </ListGroupItem>

        </ListGroup>
    );
}

export default Navabar;