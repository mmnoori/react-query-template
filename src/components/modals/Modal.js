import {useContext, useState} from 'react'
import {
    useQuery,
    useMutation,
    useQueryClient,
    QueryClient,
    QueryClientProvider,
} from 'react-query'

import {
    ListGroupItem, Row, Col, Nav, NavItem, NavLink, Button,
    ListGroup, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, FormFeedback, Input, FormText,
} from 'reactstrap'


import {consts, QUERY_KEYS,} from "../../config/consts"
import {handleInputHook,} from "../../config/config"
import {ModalContext, ModalContextInitialData, FormContext, FormContextInitialData} from "../../config/contexts"
import FormsManager from '../Forms/FormsManager'

// import UserFormWithReactHook from '../Forms/UserFormWithReactHook'


function ModalComponent() {

    function closeModal() {
        setModalData(ModalContextInitialData)
    }

    // Handle Modal Context For Providing
    const {modalData, setModalData} = useContext(ModalContext)
    const [formData, setFormData] = useState(FormContextInitialData)


    let {state, modalType, repo} = modalData


    // Modal is Closed
    if (state === consts.MODAL_CLOSED) return <></>


    let title, content

    // Choosing to show which title for modal and which FormComponent based on modalType
    // switch (modalType) {
    //     case consts.MODAL_NEW_POST:
    //         title = 'پست جدید'
    //         // content = <PostForm />
    //         content = <FormsManager modalType={modalType}/>
    //         break
    //
    //     case consts.MODAL_EDIT_POST:
    //         title = 'ویرایش پست'
    //         // content = <PostForm post={repo} />
    //         content = <FormsManager modalType={modalType} post={repo}/>
    //         break
    //
    //     case consts.MODAL_NEW_USER:
    //         title = 'کاربر جدید'
    //         content = <UserFormWithReactHook  modalType={modalType}/>
    //         break
    //
    //     case consts.MODAL_EDIT_USER:
    //         title = 'ویرایش کاربر'
    //         content = <UserFormWithReactHook modalType={modalType} user={repo}/>
    //         break
    //
    //     case consts.MODAL_DELETE:
    //         title = 'حذف آیتم'
    //         content = <div>آیا از حذف آیدی {repo.id} مطمن هستید؟</div>
    //         break
    // }


    // Modal Opened
    return (
        <>
            {/*<FormContext.Provider value={{formData, setFormData}}>*/}

                <Modal isOpen={true} toggle={closeModal}>
                    {/* toggle is on outside click */}

                    {/* toggle is click on close button */}
                    <ModalHeader toggle={closeModal}>{title}</ModalHeader>

                    <ModalBody>

                        <FormsManager/>

                        {/*{content}*/}

                    </ModalBody>

                </Modal>

            {/*</FormContext.Provider>*/}

        </>
    )
}

export default ModalComponent