import React, {useContext} from 'react';
import {Button} from "reactstrap";

import {ModalContext} from "../../config/contexts";
import {consts} from "../../config/consts";


const EditButtons = (props) => {

    const {modalData, setModalData} = useContext(ModalContext)

    // listType: is the type of list that buttons are used in
    // repo: contains data object of clicked row
    const {listType, repo} = props.value

    function openEditModal() {

        let modalType

        // opening modals based on listType
        if (listType === consts.TYPE_POST) modalType = consts.MODAL_EDIT_POST
        else if (listType === consts.TYPE_USER) modalType = consts.MODAL_EDIT_USER

        setModalData({listType, state: consts.MODAL_OPENED, modalType, repo})
    }

    function openDeleteModal() {
        // deleteModal is common between all lists
        setModalData({listType, state: consts.MODAL_OPENED, modalType: consts.MODAL_DELETE, repo})
    }


    return <>
        <Button onClick={() => openEditModal()}>ادیت</Button>
        <Button onClick={() => openDeleteModal()}>حذف</Button>
    </>

}

export default EditButtons