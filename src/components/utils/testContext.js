import { useState, createContext, useContext } from "react";
import ReactDOM from "react-dom/client";

const UserContext = createContext();

// THIS FILE IS FOR TEST

const initialData = {
    string: 'a',
    num: 0
}

export function Component1() {

    const [data, setData] = useState(initialData);

    return (
        <UserContext.Provider value={{ data, setData }}>

            <h2 onClick={() =>
                setData(
                    {
                        // ...obj,
                        // data: {
                        num: data.num + 1,
                        string: data.string + 'a'
                        // },
                    })
            }>
                {`Click To Update`}</h2>

            <div>{data.num}</div>
            <div>{data.string}</div>

            <Component2 />
        </UserContext.Provider>
    );
}

function Component2() {
    return (
        <>
            <div>Component 2</div>
            <Component3 />
        </>
    );
}

function Component3() {
    return (
        <>
            <div>Component 3</div>
            <Component4 />
        </>
    );
}

function Component4() {
    return (
        <>
            <div>Component 4</div>
            <Component5 />
        </>
    );
}

function Component5() {

    // const data = useContext(UserContext);
    const context = useContext(UserContext);

    return (
        <>
            <div>Component 5</div>

            <h2 onClick={() => context.setData(initialData) }>Click To Reset</h2>

            <h2 >{`Hello ${context.data.string} ${context.data.num} times again!`}</h2>
        </>
    );
}