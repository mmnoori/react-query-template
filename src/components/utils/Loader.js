import { Table, Button, Spinner } from 'reactstrap';
import { useIsFetching, useIsMutating } from 'react-query'



function Loader() {

    // How many queries are fetching?
    const isFetching = useIsFetching()
    const isMutating = useIsMutating()

    // How many queries matching the posts prefix are fetching?
    // const isFetchingPosts = useIsFetching([QUERY_KEYS.POSTS])

    // Log Test
    // console.log('How many queries are fetching? ')
    // console.log(isFetching)

    // console.log('How many mutations are mutating? ')
    // console.log(isMutating)

    // console.log('How many queries matching the posts prefix are fetching?')
    // console.log(isFetchingPosts)


    if (isFetching > 0 || isMutating > 0)
        return (
            <div className="loader">

                <Spinner color="primary" type="grow">
                    Loading...
                </Spinner>

                <Spinner color="secondary" type="grow">
                    Loading...
                </Spinner>

                <Spinner color="success" type="grow" >
                    Loading...
                </Spinner>

                <Spinner color="danger" type="grow">
                    Loading...
                </Spinner>

                <Spinner color="warning" type="grow">
                    Loading...
                </Spinner>

                <Spinner color="info" type="grow" >
                    Loading...
                </Spinner>

                <Spinner color="light" type="grow" >
                    Loading...
                </Spinner>

                <Spinner color="dark" type="grow" >
                    Loading...
                </Spinner>

            </div>
        );

    return <></>
}



export default Loader;
