import { createBrowserRouter, RouterProvider } from "react-router-dom";


import MainLayout from '../Layouts/MainLayout'
import { ROUTES } from "../../config/consts";
import PostsWithGrid from "../Lists/PostsWithGrid";
import UsersWithGrid from "../Lists/UsersWithGrid";



const router = createBrowserRouter([
    {
        path: ROUTES.HOME,
        element: <MainLayout />,
        //   errorElement: <ErrorPage />,
        // loader: rootLoader(queryClient),
        // action: rootAction(queryClient),
        children: [
            {
                index: true,
                element: <div>NOTHING HERE </div>,
            },
            {
                path: ROUTES.USERS,
                // element: <Users />,
                element: <UsersWithGrid />,
                // loader: contactLoader(queryClient),
                // action: contactAction(queryClient),
            },
            {
                path: ROUTES.POSTS,
                // element: <Posts />,
                element: <PostsWithGrid />,
                // loader: contactLoader(queryClient),
                // action: contactAction(queryClient),
            },
        ],
    },
]);





function RouteManager() {
    return (
        <>
            <RouterProvider router={router} />
        </>
    );
}

// Routing without RouterProvider
// function RouteManager() {
//     return (
//         <Container>

//             <Routes>
//                 <Route path="/" element={<MainLayout />}>

//                     <Route index element={<div>HOME</div>} />
//                     <Route path="users" element={<Users />} />
//                     <Route path="posts" element={<Posts />} />

//                     {/* Using path="*"" means "match anything", so this route
//                           acts like a catch-all for URLs that we don't have explicit
//                           routes for. */}
//                     {/* <Route path="*" element={<NoMatch />} /> */}
//                 </Route>
//             </Routes>

//         </Container>
//     );
// }

export default RouteManager;
