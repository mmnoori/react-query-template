import React, {useContext} from 'react';
import {FormGroup, Label} from "reactstrap";

import {alerts, consts, FORMS, VALIDATIONS} from "../../config/consts";
import {FormContext, ModalContext} from "../../config/contexts";
import FormHookValidator from "../validator/FormHookValidator";


const UserForm = () => {

    // Modal Context
    const {modalData, setModalData} = useContext(ModalContext)
    const { modalType, repo } = modalData

    // Using ReactHookForm through Form Context
    const {ReactHookForm} = useContext(FormContext)
    const {register, errors} = ReactHookForm

    // console.log(repo)

    // if Deleting a User
    if (modalType === consts.MODAL_DELETE)
        return <div>آیا از حذف کاربر آیدی {repo.id} اطمینان دارید؟</div>

    // if add/editing a User
    return (
        <>

            <FormGroup>
                <Label>نام</Label>
                <input className='form-control' type="text" placeholder="نام را وارد کنید"
                       {...register(FORMS.NAME, {
                           required: true,
                           maxLength: consts.USER_NAME_MAX_LENGTH,
                           minLength: consts.USER_NAME_MIN_LENGTH,
                           pattern: /^([^0-9]*)$/
                       })}
                />
            </FormGroup>

            <FormHookValidator errors={errors} name={FORMS.NAME} options={[VALIDATIONS.REQUIRED, VALIDATIONS.MIN_LENGTH, VALIDATIONS.MAX_LENGTH, VALIDATIONS.PATTERN]}/>



            <FormGroup>
                <Label>نام کاربری</Label>
                <input className='form-control' type="text" placeholder="نام کاربری را وارد کنید"
                       {...register(FORMS.USERNAME, {
                           required: true,
                           maxLength: consts.USER_USERNAME_MIN_LENGTH,
                           minLength: consts.USER_USERNAME_MIX_LENGTH,
                       })}
                />
            </FormGroup>

            <FormHookValidator errors={errors} name={FORMS.USERNAME} options={[VALIDATIONS.REQUIRED, VALIDATIONS.MIN_LENGTH, VALIDATIONS.MAX_LENGTH]}/>


            <FormGroup>
                <Label>ایمیل</Label>
                <input className='form-control' type="text" placeholder="ایمیل را وارد کنید"
                       {...register(FORMS.EMAIL, {
                           required: true,
                       })}
                />
            </FormGroup>

            <FormHookValidator errors={errors} name={FORMS.EMAIL} options={[VALIDATIONS.REQUIRED,]}/>




            <FormGroup>
                <Label>شماره تماس</Label>
                <input className='form-control' type="number" placeholder="شماره تماس را وارد کنید"
                       {...register(FORMS.PHONE, {
                           required: true,
                           maxLength: consts.USER_PHONE_MAX_LENGTH,
                           minLength: consts.USER_PHONE_MIN_LENGTH,
                       })}
                />
            </FormGroup>

            <FormHookValidator errors={errors} name={FORMS.PHONE} options={[VALIDATIONS.REQUIRED, VALIDATIONS.MAX_LENGTH, VALIDATIONS.MIN_LENGTH]}/>



            <FormGroup>
                <Label>وبسایت</Label>
                <input className='form-control' type="text" placeholder="وبسایت"
                       {...register(FORMS.WEBSITE, {
                           required: true,
                           maxLength: consts.POST_TITLE_MAX_LENGTH,
                           minLength: consts.POST_TITLE_MIN_LENGTH,
                       })}
                />
            </FormGroup>

            <FormHookValidator errors={errors} name={FORMS.WEBSITE} options={[VALIDATIONS.REQUIRED, VALIDATIONS.MAX_LENGTH, VALIDATIONS.MIN_LENGTH]}/>

        </>
    )
}

export default UserForm
