import React, {useContext} from 'react';
import {FormGroup, Label} from "reactstrap";

import {alerts, consts, FORMS, VALIDATIONS} from "../../config/consts";
import {FormContext, ModalContext} from "../../config/contexts";
import FormHookValidator from '../validator/FormHookValidator';


const PostForm = () => {

    // Modal Context
    const {modalData, setModalData} = useContext(ModalContext)
    const { modalType, repo } = modalData

    // Using ReactHookForm through Form Context
    const {ReactHookForm} = useContext(FormContext)
    const {register, errors} = ReactHookForm


    // if Deleting a Post
    if (modalType === consts.MODAL_DELETE)
        return <div>آیا از حذف پست آیدی {repo.id} اطمینان دارید؟</div>

    // if add/editing a post
    return (
        <>
            <FormGroup>

                <Label>یوز آیدی</Label>

                {/* NOTE: react-hook-form does NOT WORK with ReactStrap Input  */}
                <input className='form-control' type="number"
                       {...register(FORMS.USER_ID,
                           {
                               required: true,
                               maxLength: consts.USER_ID_MAX_LENGTH,
                               minLength: consts.USER_ID_MIN_LENGTH,
                           })}
                />
            </FormGroup>

            <FormHookValidator errors={errors} name={FORMS.USER_ID} options={[VALIDATIONS.REQUIRED, VALIDATIONS.MIN_LENGTH, VALIDATIONS.MAX_LENGTH]}/>

            {/*{errors?.userId?.type === "required" && <p>{alerts.ALERT_REQUIRED}</p>}*/}
            {/*{errors?.userId?.type === "maxLength" && (<p>{alerts.ALERT_MAX_LENGTH}</p>)}*/}
            {/*{errors?.userId?.type === "minLength" && (<p>{alerts.ALERT_MIN_LENGTH}</p>)}*/}


            <FormGroup>
                <Label>تایتل</Label>
                <input className='form-control' type="text" placeholder="تایتل را وارد کنید"
                       {...register(FORMS.TITLE, {
                           required: true,
                           maxLength: consts.POST_TITLE_MAX_LENGTH,
                           minLength: consts.POST_TITLE_MIN_LENGTH,
                           // pattern: /^[A-Za-z]+$/i
                           pattern: /^([^0-9]*)$/
                       })}
                />
            </FormGroup>

            <FormHookValidator errors={errors} name={FORMS.TITLE} options={[VALIDATIONS.REQUIRED, VALIDATIONS.MIN_LENGTH, VALIDATIONS.MAX_LENGTH, VALIDATIONS.PATTERN]}/>


            <FormGroup>
                <Label>متن</Label>

                {/*<input className='form-control' placeholder="متن را وارد کنید"*/}
                {/*       {...register("body", {required: true, maxLength: 100, minLength: 20})}*/}
                {/*/>*/}

                <textarea className='form-control' placeholder="متن را وارد کنید"
                          {...register(FORMS.BODY,
                              {
                                  required: true,
                                  maxLength: consts.POST_BODY_MAX_LENGTH,
                                  minLength: consts.POST_BODY_MIN_LENGTH
                              })}
                />
            </FormGroup>

            <FormHookValidator errors={errors} name={FORMS.BODY} options={[VALIDATIONS.REQUIRED, VALIDATIONS.MIN_LENGTH, VALIDATIONS.MAX_LENGTH]}/>

        </>
    )
}

export default PostForm


// Previous Form Without React Hook Form
// function PostForm(props) {
//
//     // Receiving post as props when editing a post
//     const { post } = props
//
//     let { formData, setFormData } = useContext(FormContext)
//
//     // DidMount
//     useEffect(() => {
//         setFormData({
//             userId : (post) ? post.userId : '',
//             title : (post) ? post.title : '',
//             body : (post) ? post.body : ''
//         })
//     }, [])
//
//
//     return (
//         <Form >
//
//             <FormGroup>
//                 <Label >
//                     یوزر آیدی
//                 </Label>
//                 <Input
//                     name="userId"
//                     type='number'
//                     value={formData.userId}
//                     maxLength={consts.USER_ID_MAX_LENGTH}
//                     onChange={(e) => handleInputHook(e, formData, setFormData)}
//                 />
//
//             </FormGroup>
//
//             <FormGroup>
//                 <Label >
//                     تایتل
//                 </Label>
//                 <Input
//                     name="title"
//                     type="textarea"
//                     value={formData.title}
//                     maxLength={consts.POST_TITLE_MAX_LENGTH}
//                     onChange={(e) => handleInputHook(e, formData, setFormData)}
//                 />
//             </FormGroup>
//
//             <FormGroup>
//                 <Label >
//                     متن
//                 </Label>
//                 <Input
//                     name="body"
//                     type="textarea"
//                     value={formData.body}
//                     maxLength={consts.POST_BODY_MAX_LENGTH}
//                     onChange={(e) => handleInputHook(e, formData, setFormData)}
//                 />
//             </FormGroup>
//
//         </Form>
//     )
// }