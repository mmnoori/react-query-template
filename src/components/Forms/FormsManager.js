import {useContext, useEffect, useState,} from 'react'
import {
    ListGroupItem, Row, Col, Nav, NavItem, NavLink, Button,
    ListGroup, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, FormFeedback, Input, FormText,
} from 'reactstrap';
import {useForm} from "react-hook-form";


import {alerts, consts, QUERY_KEYS,} from "../../config/consts";
import {handleInputHook,} from "../../config/config";
import {FormContext, ModalContext, ModalContextInitialData} from '../../config/contexts';
import Loader from "../utils/Loader";
import {useMutation, useQueryClient} from "react-query";
import {getItemMutationOptions, itemFunctionFn, postMutation, postMutationOptions} from "../../RQ/MutationHelper";
import PostForm from "./PostForm";
import UserForm from "./UserForm";


function FormsManager() {

    // Modal Context
    const {modalData, setModalData} = useContext(ModalContext)
    const {listType, modalType, repo} = modalData


    let queryKey, form

    // choose Which Form to show and queryKey from type
    switch (listType) {
        case consts.TYPE_POST:
            form = <PostForm/>
            queryKey = QUERY_KEYS.POSTS
            break

        case consts.TYPE_USER:
            form = <UserForm/>
            queryKey = QUERY_KEYS.USERS
            break
    }


    // List Mutation
    const queryClient = useQueryClient()
    const listMutation = useMutation(...getItemMutationOptions(queryClient, queryKey))


    // React Hook Form
    const {register, handleSubmit, watch, formState: {errors}} = useForm({
        defaultValues: getFormDefaultValues(listType, repo)
    })


    function callMutation(requestMethod, formData) {
        listMutation.mutate({listType, requestMethod, repo, formData, callback: closeModal})
    }

    // When User Confirms a Form
    function handleConfirm(formData) {

        let requestMethod

        switch (modalType) {
            case consts.MODAL_NEW_POST:
                requestMethod = 'post'
                callMutation(requestMethod, formData)
                break

            case consts.MODAL_EDIT_POST:
                requestMethod = 'put'
                callMutation(requestMethod, formData)
                break

            case consts.MODAL_NEW_USER:
                requestMethod = 'post'
                callMutation(requestMethod, formData)
                break

            case consts.MODAL_EDIT_USER:
                requestMethod = 'put'
                callMutation(requestMethod, formData)
                break

            case consts.MODAL_DELETE:
                requestMethod = 'delete'
                callMutation(requestMethod, formData)
                break
        }
    }


    // React Hook Form onSubmit :
    // Will be executed after validation
    const onSubmit = (data) => {
        handleConfirm(data)
    }

    function closeModal() {
        setModalData(ModalContextInitialData)
    }

    return (
        <FormContext.Provider value={{ReactHookForm: {register, errors}}}>

            {/* Using Loader in ModalBody Because Loader can not be simply shown over modal by CSS ZIndex */}
            <Loader/>

            {/* React Form Hook only works with form submit  */}
            <Form onSubmit={handleSubmit(onSubmit)}>


                {form}


                {/* react-hook-form handleSubmit does NOT WORK with ReactStrap Button onClick  */}
                {/* <Button color="primary" onClick={() => handleSubmit(onSubmit)}>ثبت</Button> */}

                {/* By removing onClick attr, ReactStrap Button submits form  */}
                <Button color="primary">
                    ثبت
                </Button>

                {/* Close Modal */}
                <Button color="secondary" onClick={() => setModalData(ModalContextInitialData)}>
                    بستن
                </Button>


                {/* <button className='btn btn-primary' color="primary" type="submit" >ثبت</button> */}
                {/* <Input type="submit" /> */}

            </Form>

        </FormContext.Provider>
    )
}

// Default Values To Show When Form Is Opened
function getFormDefaultValues(listType, object) {
    if (listType === consts.TYPE_POST)
        return {
            userId: object ? object.userId : '',
            title: object ? object.title : '',
            body: object ? object.body : '',
        }

    if (listType === consts.TYPE_USER)
        return {
            id: object ? object.id : '',
            name: object ? object.name : '',
            username: object ? object.username : '',
            email: object ? object.email : '',
            phone: object ? object.phone : '',
            website: object ? object.website : '',
        }
}

export default FormsManager