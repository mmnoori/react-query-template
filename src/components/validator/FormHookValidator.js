import React from 'react';
import {alerts, VALIDATIONS} from "../../config/consts";

const FormHookValidator = ({errors, name, options}) => {


    return (
        <>
            {
                options.map((option, index) => {

                    if (errors?.[name]?.type === option)
                        return <p>{getValidationError(option)}</p>
                })
            }
        </>
    );
};


function getValidationError(validatorOption) {

    switch (validatorOption) {
        case VALIDATIONS.REQUIRED:
            return <p>{alerts.ALERT_REQUIRED}</p>

        case VALIDATIONS.MAX_LENGTH:
            return <p>{alerts.ALERT_MAX_LENGTH}</p>

        case VALIDATIONS.MIN_LENGTH:
            return <p>{alerts.ALERT_MIN_LENGTH}</p>

        case VALIDATIONS.PATTERN:
            return <p>{alerts.ALERT_ONLY_ALPHABETICAL}</p>
    }
}

export default FormHookValidator