import { Routes, Route, Outlet, Link } from "react-router-dom";
import { useState } from 'react'

import {
    useQuery,
    useMutation,
    useQueryClient,
    QueryClient,
    QueryClientProvider,
} from 'react-query'
import { ListGroup, ListGroupItem, Row, Col, Nav, NavItem, NavLink, Button } from 'reactstrap';

import { useGetFetchQuery } from '../../RQ/Queries'
import { consts, QUERY_KEYS } from '../../config/consts'
import Loader from '../utils/Loader'
import ModalComponent from "../modals/Modal";
import { Component1 } from "../utils/testContext";
import { ModalContext, ModalContextInitialData } from "../../config/contexts";
import ToastComponent from '../utils/Toast';
import Navabar from "../Navabar";




function MainLayout() {

    // Handle Modal State
    let [modalData, setModalData] = useState(ModalContextInitialData)

    function openNewPostModal() {
        setModalData({listType: consts.TYPE_POST, state: consts.MODAL_OPENED, modalType: consts.MODAL_NEW_POST })
    }


    return (
        <div className='layout-con'>

            {/* if have multiple providers for SAME Context, the provider from deeper layer will take effect */}
            <ModalContext.Provider value={{ modalData, setModalData }}>


                <Button className="new-button" onClick={openNewPostModal}>
                    پست جدید
                </Button>

                <hr />

                <div className="main">

                    <Loader />

                    <ModalComponent />

                    <ToastComponent />

                    {/* <Component1/> */}

                    <Navabar />


                    {/* An <Outlet> renders whatever child route is currently active,
                    so you can think about this <Outlet> as a placeholder for
                    the child routes we defined above. */}
                    <Outlet />

                </div>

            </ModalContext.Provider>

        </div>
    )
}


export default MainLayout
