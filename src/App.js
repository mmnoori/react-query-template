import {useState, createContext} from "react"
import ReactDOM from "react-dom/client"

// import { ReactQueryDevtools } from '@tanstack/react-query-devtools'
import { ReactQueryDevtools } from 'react-query/devtools'; // This Works


import RouteManager from './components/Router/RouteManager'

import {
    useQuery,
    useMutation,
    useQueryClient,
    QueryClient,
    QueryClientProvider,
} from 'react-query'


// Stale queries are refetched automatically in the background when:
// New instances of the query mount
// The window is refocused
// The network is reconnected.
// The query is optionally configured with a refetch interval.

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            // staleTime: 1000 * 10,
            refetchOnWindowFocus: false,
        },
    },
})

// Invalidate every query in the cache
queryClient.invalidateQueries()

function App() {
    return (

        // <QueryClientProvider client={queryClient} contextSharing={true}>
        <QueryClientProvider client={queryClient}>

            <RouteManager/>

            {/* <FormHookTest /> */}

            {/* <ReactQueryDevtools initialIsOpen={false} position="bottom-right" /> */}
            <ReactQueryDevtools initialIsOpen={false} />

        </QueryClientProvider>
    )
}

export default App;
