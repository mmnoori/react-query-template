
import {axiosRequest, wait} from '../config/config'


// React Hook "useMutation" cannot be called at the top level.
// React Hooks must be called in a React function component or a custom React Hook function


// listType: users or posts or ...
export function itemFunctionFn({listType, requestMethod, repo, formData, callback}) {

    return new Promise((resolve, reject) => {

        let data = {}

        // Received data from post form
        if (formData) data = formData

        let url = `https://jsonplaceholder.typicode.com/${listType}/${(repo) ? repo.id : ''}`

        // Just making promise a little bit longer for TEST purposes
        wait(500).then(() => {

            axiosRequest({method: requestMethod, url, data}, () => {
                resolve()
                callback()
            })

        })
    })
}

export function getItemMutationOptions(queryClient, queryKey) {
    return (
        [
            itemFunctionFn, {

            onMutate: async data => {

                // console.log(`onMutate`)
                // console.log(`data : `)
                // console.log(data)

                // Optimistically update the cache value on mutate, but store
                // the old value and return it so that it's accessible in case of
                // an error
                // setText('')
                // await queryClient.cancelQueries('todos')
                //
                // const previousValue = queryClient.getQueryData('todos')
                //
                // queryClient.setQueryData('todos', old => ({
                //     ...old,
                //     items: [...old.items, data],
                // }))
                //
                // return previousValue
            },
            onError: (err, variables, previousValue) => {
                // console.log('Mutation Error')

                // On failure, roll back to the previous value
                // queryClient.setQueryData('todos', previousValue),
            },

            // After success or failure, refetch the todos query
            onSettled: () => {
                // console.log('Mutation onSettled')

                queryClient.invalidateQueries(queryKey)
            },
        }
        ]
    )
}


