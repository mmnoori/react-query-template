import axios from 'axios'
import {
    useQuery,
    useMutation,
    useQueryClient,
    QueryClient,
    QueryClientProvider,
} from 'react-query'


import { consts, QUERY_KEYS } from '../config/consts'

// NOTE: 
 // A query is a declarative dependency on an asynchronous source of data that is tied to a unique key.
 // A query can be used with any Promise based method (including GET and POST methods) to fetch data from a server.
 // If your method modifies data on the server, we recommend using Mutations instead.


export function useLoading(bool) {
    return useQuery({
        queryKey: [QUERY_KEYS.LOADING],
        queryFn: async () => {
            return bool
        },
    });
}

export function useModal(open) {

    return useQuery({
        queryKey: [QUERY_KEYS.MODAL],
        queryFn: async () => {

            // if (open === undefined)
            //     return consts.MODAL_CLOSED;

            if (open === true)
                return consts.MODAL_OPENED;

            else if (open === false)
                return consts.MODAL_CLOSED;
        },
        initialData: consts.MODAL_OPENED
    });
}

export function useModalState(modalState) {

    return useQuery({
        queryKey: [QUERY_KEYS.MODAL],
        queryFn: async () => {
            return !modalState
        },
    })
}

// if use OneKey & an Object for value
// Need Previous Object when updating multiple fields like :  {...object, something chnaged}
export function useFormState(name, ) {

    return useQuery({
        queryKey: [QUERY_KEYS.FORM_STATE],
        queryFn: async () => {
            return {}
        },
    })

}

export function usePosts() {

    return useQuery({
        queryKey: [QUERY_KEYS.POSTS],
        queryFn: async () => {
            const { data } = await axios.get(
                "https://jsonplaceholder.typicode.com/posts"
            );

            return data;
        },
        initialData: [],
        // staleTime: 1000,
    });
}

export function useUsers() {

    return useQuery({
        queryKey: [QUERY_KEYS.USERS],
        queryFn: async () => {
            const { data } = await axios.get(
                "https://jsonplaceholder.typicode.com/users"
            );
            return data;
        },
        initialData: []
    });
}



// returns data from query
export const useGetFetchQuery = (key) => {
    const queryClient = useQueryClient();

    // might set default data states here
    // check if value is not already set
    // and set default value


    // otherwise return value
    let queryData = queryClient.getQueryData(key);


    // console.log(queryData)

    if (queryData === undefined) {

        queryData = {}

        // Switchcase for setting default values
        switch (key) {
            case QUERY_KEYS.MODAL:
                queryData.data = consts.MODAL_CLOSED
                break
        }
    }

    return (queryData)
};

