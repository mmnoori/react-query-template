export const QUERY_KEYS = {
    POSTS: 'posts',
    USERS: 'users',
}

export const consts = {

    COLUMN_ID: 'آیدی',
    COLUMN_USER_ID: 'یوزر آیدی',
    COLUMN_TITLE: 'عنوان',
    COLUMN_BODY: 'متن',
    COLUMN_ACTIONS: 'اکشن',

    COLUMN_NAME: 'نام',
    COLUMN_USERNAME: 'نام کاربری',
    COLUMN_EMAIL: 'ایمیل',
    COLUMN_PHONE: 'شماره',
    COLUMN_WEBSITE: 'وبسایت',


    TYPE_POST: 'posts',
    TYPE_USER: 'users',


    MODAL_OPENED: 'opened',
    MODAL_CLOSED: 'closed',

    MODAL_NEW_POST: 'NewPost',
    MODAL_EDIT_POST: 'EditPost',

    MODAL_NEW_USER: 'NewUser',
    MODAL_EDIT_USER: 'EditUser',

    MODAL_DELETE: 'DeleteItem',


    USER_ID_MIN_LENGTH: 2,
    USER_ID_MAX_LENGTH: 5,
    POST_TITLE_MIN_LENGTH: 10,
    POST_TITLE_MAX_LENGTH: 60,
    POST_BODY_MIN_LENGTH: 20,
    POST_BODY_MAX_LENGTH: 200,

    USER_NAME_MIN_LENGTH: 10,
    USER_NAME_MAX_LENGTH: 60,
    USER_USERNAME_MIX_LENGTH: 10,
    USER_USERNAME_MIN_LENGTH: 60,

    USER_PHONE_MIN_LENGTH: 10,
    USER_PHONE_MAX_LENGTH: 11,
}


export const alerts = {
    ALERT_REQUIRED: 'این فیلد اجباری است',
    ALERT_MAX_LENGTH: 'تعداد کارکتر ها بیش از حد مجاز است',
    ALERT_MIN_LENGTH: 'تعداد کارکتر ها کمتر از حد مجاز است',
    ALERT_NO_NUMBERS: 'نمیتوانید از اعداد استفاده کنید',
    ALERT_ONLY_ALPHABETICAL: 'فقط حروف الفبا مجاز است',
}

export const ROUTES = {
    HOME: '/',
    POSTS: '/posts',
    USERS: '/users',
}

export const FORMS = {
    USER_ID: "userId",
    TITLE: "title",
    BODY: "body",
    NAME: "name",
    USERNAME: "username",
    EMAIL: "email",
    PHONE: "phone",
    WEBSITE: "website",
}

export const VALIDATIONS = {
    REQUIRED: 'required',
    MAX_LENGTH: 'maxLength',
    MIN_LENGTH: 'minLength',
    PATTERN: 'pattern'
}