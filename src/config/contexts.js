import React from 'react'
import {consts} from './consts';

// Modal State
export const ModalContextInitialData = {
    state: consts.MODAL_CLOSED,
    modalType: '',
    repo: {},
}

// Forms State
export const FormContextInitialData = {}

export const ModalContext = React.createContext(ModalContextInitialData);
export const FormContext = React.createContext(FormContextInitialData);

// export const ModalContext = React.createContext(null);

