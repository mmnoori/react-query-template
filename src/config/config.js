import axios from 'axios'

export const log = function (message) {
    console.log(message)
}

export const toType = function (obj) {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}

export function wait(latency) {
    return new Promise((resolve) => setTimeout(() => {
        resolve()
    }, latency))
}

// this function is for handling inputs manually
export function handleInputHook(event, prevState, setValue) {

    let target = event.target
    let value = target.type === 'checkbox' ? target.checked : target.value
    let name = target.name

    if (value.length > target.maxLength) return // if value exceeds maxLength, no need to update

    // setValue(event.target.value);
    setValue({...prevState, [name]: event.target.value});
}

export function axiosRequest({url, method, data}, callback) {

    return axios({method, url, data}).then(() =>
        callback()
    )
}

export function validator() {

}




